package com.example.englishforkids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button level1, level2, level3;

Intent intent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        level1=(Button)findViewById(R.id.btn_level1);
        level2=(Button)findViewById(R.id.btn_level2);
        level3=(Button)findViewById(R.id.btn_level3);


    }

    @Override
    public void onClick(View v) {
        if(v.getId()==level1.getId())
        {
            intent=new Intent(this,DetailActivity.class);
            startActivity(intent);
        } else if (v.getId()==level2.getId()){
            intent=new Intent(this,DetailActivity.class);
            startActivity(intent);
        }else {
            intent=new Intent(this,DetailActivity.class);
            startActivity(intent);
        }
    }




}
