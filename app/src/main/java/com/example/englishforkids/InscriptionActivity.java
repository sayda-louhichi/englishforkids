package com.example.englishforkids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class InscriptionActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_Inscription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        btn_Inscription=(Button)findViewById(R.id.btn_inscription);
btn_Inscription.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if(v.getId()==btn_Inscription.getId())
        {
            Intent intent = new Intent(InscriptionActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }


    }



